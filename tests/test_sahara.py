import pytest
from subprocess import Popen, PIPE, STDOUT

def test_answer():
    p = Popen(['./mybinary'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)    
    stdout = p.communicate(input=b'+\n3 4')[0]
    assert stdout.decode().split(' ')[-1].strip() == '7'
